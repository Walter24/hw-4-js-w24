function fib(f0,f1,n) {
  /* Послідовність додатніх чисел Фібоначі - базовий ряд:  0,1,1,2,3,5,8,13,21,34,55,89,...
   Рахунок n-го числа ведеться від 1, принцип створення ряду Фібоначі: F2 = F0 + F1, F3 = F1 + F2 ...*/
  let result = 0;
  if (n == 0) return f0;
    if(n == 1) return f1;

  else if (n > 2) {
    for (i = 2; i < n; i++) {  //  перше(0) і друге (1) числа вже визначенні як змінні, а тому починаємо з наступного числа (2)
      result = f0 + f1; // 
      f0 = f1;
      f1 = result;
    }
  }

  else {
    for (i = -1; i > n; i--) {  //  перше число (0) вже визначено, а тому починаємо з -1го числа
      result = f0 - f1;
      f0 = f1;
      f1 = result;
    }
  }
  return result;
}

let f0 = +prompt("Enter the first number, integer", "2");
let f1 = +prompt("Enter the second number, integer", "4");
let n = +prompt("Enter the sequence Fibonacci number", "9");

alert( fib(f0,f1,n) + " " + ":" + " " + "this is your Fibonacci Number");


// Перевірка роботи функції для підрахунку узагальненого n-го числа Фібоначі
// alert( fib(2,4,9) );  // виведе 110
// alert( fib(3,5,7) );  // виведе 55
// alert( fib(-1,2,-4) ); // виведе -8
// alert( fib(5,-8,-3) ); // виведе -21